package com.trashlabs.lime.test;

import com.trashlabs.lime.DataEntity;
import com.trashlabs.lime.annotations.GeneratedValue;
import com.trashlabs.lime.annotations.PrimaryKey;

public class Person extends DataEntity {
	
	@PrimaryKey
	@GeneratedValue
	public long id; 
	
	public String lastName; 
	
	public String firstName; 

}
