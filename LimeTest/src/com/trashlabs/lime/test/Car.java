package com.trashlabs.lime.test;

import com.trashlabs.lime.DataEntity;
import com.trashlabs.lime.annotations.GeneratedValue;
import com.trashlabs.lime.annotations.OneToOne;
import com.trashlabs.lime.annotations.PrimaryKey;

public class Car extends DataEntity {
	
	@GeneratedValue
	@PrimaryKey
	public long id; 
	public String make; 
	public String model;
	public long personId; 
	
	@OneToOne(child=Person.class, parentKey="personId", childKey="id")
	public Person person; 
}
