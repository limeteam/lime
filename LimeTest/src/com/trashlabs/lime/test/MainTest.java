package com.trashlabs.lime.test;

import java.util.List;

import com.trashlabs.lime.DataList;
import com.trashlabs.lime.test.Contact; 

import android.database.Cursor;
import android.provider.ContactsContract.RawContacts;
import android.test.AndroidTestCase;
import android.text.TextUtils;
import android.util.Log;

public class MainTest extends AndroidTestCase {
	
	AppDb db; 
	
	@Override
	protected void setUp() throws Exception {
		db = new AppDb(getContext());
	}
	
	public void testAdd() throws Exception {
		Person p1 = new Person(); 
		p1.lastName = "Mouse";
		p1.firstName = "Mickey";
		long id = db.people.add(p1);
		p1 = db.people.find(id);
		assertEquals(p1.lastName, "Mouse");
	}
	
	public void testOneToOne() throws Exception {
		Person p1 = new Person(); 
		p1.lastName = "Mouse";
		p1.firstName = "Mickey";
		db.people.add(p1);
		
		Car c1 = new Car(); 
		c1.make = "Honda";
		c1.model = "S2000";
		c1.personId = p1.id;
		db.cars.add(c1);
		
		c1 = db.cars.query().where("make=?", c1.make).getSingle();
		assertEquals(c1.person.lastName, "Mouse");
	}
	
	public void testContentProvider() throws Exception {
		// make sure there are contacts on the device or this test will fail
		final String[] projection = new String[] { 
				RawContacts.CONTACT_ID,  
				RawContacts.DISPLAY_NAME_PRIMARY
				};
		Cursor cursor = getContext().getContentResolver().query(RawContacts.CONTENT_URI, projection,null, null,null); 
		
		List<Contact> contacts = DataList.create(Contact.class, cursor);
		
		for(Contact contact : contacts) {
			Log.d("lime", "name:" + contact.displayName);
		}
		assertTrue(contacts.size() > 0);
		Contact c = contacts.get(0);
		assertTrue(!TextUtils.isEmpty(c.displayName));
	}
}
