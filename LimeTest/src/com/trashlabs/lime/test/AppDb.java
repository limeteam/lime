package com.trashlabs.lime.test;

import android.content.Context;

import com.trashlabs.lime.DataSet;
import com.trashlabs.lime.SqlDataStore;

public class AppDb extends SqlDataStore {
	
	public DataSet<Person> people = new DataSet<Person>(this, Person.class);
	public DataSet<Car> cars = new DataSet<Car>(this, Car.class);
	
	public AppDb(Context context) {
		super(context, "example.db", 2);
	}

}