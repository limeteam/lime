package com.trashlabs.lime;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public abstract class ContentProvider extends android.content.ContentProvider {
	
	SQLiteDatabase db; 

	@Override
	public int delete(Uri uri, String whereClause, String[] whereArgs) {
		return db.delete(getTable(uri), whereClause, whereArgs);
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		long id =  db.insert(getTable(uri), null, values);
		return Uri.parse(getTable(uri) +"/" + String.valueOf(id));
	}

	@Override
	public boolean onCreate() {
		db = getDataStore(getContext()).getDb();
		return true;
	}
	
	public abstract SqlDataStore getDataStore(Context context);

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		String tableName = uri.getPathSegments().get(0);
		return db.query(false, tableName, projection, selection, selectionArgs, null, null, sortOrder, null);
	}

	@Override
	public int update(Uri uri, ContentValues values, String whereClause, String[] whereArgs) {
		return db.update(getTable(uri), values, whereClause, whereArgs);
	}
	
	private String getTable(Uri uri) {
		return uri.getPathSegments().get(0);
	}

}
