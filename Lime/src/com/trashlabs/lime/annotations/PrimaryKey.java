package com.trashlabs.lime.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotate a column primary key but not database generated. For a database
 * generated key use the AutoIncrement annotation. 
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PrimaryKey { }