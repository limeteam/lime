package com.trashlabs.lime;

import com.trashlabs.lime.annotations.GeneratedValue;

/**
 * DataEntity is the base class for all Sql storable entities. This class allows the entity to 
 * be loaded including the _rowid_ for delete operations, etc. 
 */
public class DataEntity {
	
	/**
	 * Sqlite issued id for the row. 
	 */
	@GeneratedValue
	public long _rowid_ = -1;

}
