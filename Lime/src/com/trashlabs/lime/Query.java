package com.trashlabs.lime;

import java.util.List;

import android.database.Cursor;
import android.util.Log;

import com.trashlabs.lime.sql.Select;

/**
 * The Query object wraps a Select statement and has the capability to generate a 
 * list as a result using the getList() method. A query is usually fetched from the 
 * datastore dataset. 
 * 
 * @param <T>
 */
public class Query<T extends DataEntity> {
	
	private SqlDataStore db; 
	private ClassInfo info; 
	private Select select;
	
	Query(SqlDataStore db, ClassInfo info) {
		this.db = db; 
		this.info = info; 
		select = SelectBuilder.build(info);
	}
	
	public Query<T> where(String expression) {
		select.where(expression);
		return this; 
	}
	
	public Query<T> where(String expression, String... args) {
		select.where(expression, args);
		return this; 
	}
	
	public Query<T> orderBy(String... orderBy) {
		select.orderBy(orderBy);
		return this; 
	}
	
	public Query<T> distinct() {
		select.distinct();
		return this; 
	}
	
	public Query<T> include(String fieldName, ClassInfo child, String parentKey, String childKey) {
		String table = child.getTableName() + " AS " + fieldName; 
		String expression = parentKey + "=" + fieldName + "." + childKey; 
		select.leftJoin(table, expression);
		select.addColumns(child.getBasicFieldNames(fieldName));
		return this; 
	}
	
	public List<T> getList() {
		String sql = this.toString();
		Log.d("lime", sql);
		Cursor cursor = db.rawQuery(sql, select.getArgs());
		ObjectReader<T> reader = info.getReader(db.getDb(), cursor);
		return new DataList<T>(cursor, reader);
	}
	
	public T getSingle() {
		List<T> items = getList(); 
		if(items.size() == 1) 
			return items.get(0);
		else if(items.size() == 0) 
			throw new RuntimeException("Zero items found when 1 was expected.");
		else
			throw new RuntimeException("More than 1 item was found.");
	}
	
	public T getSingleOrNull() {
		List<T> items = getList(); 
		if(items.size() == 1) 
			return items.get(0);
		else if(items.size() == 0) 
			return null; 
		else
			throw new RuntimeException("More than 1 item was found.");
	}
	
	@Override
	public String toString() {
		return select.toString();
	}
}
