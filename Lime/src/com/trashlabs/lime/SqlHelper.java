package com.trashlabs.lime;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Date;
import java.util.ArrayList;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

/**
 * This is an implementation of the SQLiteOpenHelper to allow for the lazy creation and upgrade of 
 * databases. This class inspects an instance of a SqlDataStore to find DbSet fields. Using data from 
 * those fields, it automatically handles onCreate by scripting a database. 
 */
class SqlHelper extends SQLiteOpenHelper {

	private SqlDataStore dataStore; 

	/**
	 * Construct a new SqlHelper.
	 * 
	 * @param context
	 * @param dataStore
	 * @param name
	 * @param factory
	 * @param version
	 */
	public SqlHelper(Context context, SqlDataStore dataStore, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
		this.dataStore = dataStore; 
	}

	/* (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		Field[] fields = dataStore.getClass().getFields();
		ArrayList<String> tables = new ArrayList<String>();
		for(Field field : fields) {
			boolean isDbSet = field.getType().equals(DataSet.class);
			if(!isDbSet) continue; 
			String sql;
			try {
				DataSet<?> dbset = (DataSet<?>)field.get(dataStore);
				ClassInfo info = dbset.getClassInfo();
				sql = generateCreateSql(info);
				Log.d("Data Table Create", sql);
				db.execSQL(sql);
				tables.add(info.getTableName());
			} catch (Exception e) {
				throw new RuntimeException("database table for " + field.getName() + " could not be created", e);
			}
		}
	}

	/* (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Field[] fields = dataStore.getClass().getFields();
		for(Field field : fields) {
			boolean isDbSet = field.getType().equals(DataSet.class);
			if(!isDbSet) continue; 
			String sql;
			try {
				DataSet<?> set = (DataSet<?>)field.get(dataStore);
				sql = generateDropSql(set.getClassInfo());
				Log.d("Drop Table", sql);
				db.execSQL(sql);
			} catch (Exception e) {
				throw new RuntimeException("database table for " + field.getName() + " could not be upgraded", e);
			}
		}
		onCreate(db);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.database.sqlite.SQLiteOpenHelper#onDowngrade(android.database
	 * .sqlite .SQLiteDatabase, int, int)
	 */
	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Don't call super, it throws a down grade exception.
		String path = db.getPath();
		File dbFile = new File(path);
		if (dbFile.exists()) {
			dbFile.delete();
			try {
				dbFile.createNewFile();
				onCreate(db);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Generate the sql to drop a table from the database. 
	 * 
	 * @param set
	 * @return
	 */
	private String generateDropSql(ClassInfo classInfo) {
		return "DROP TABLE IF EXISTS [" + classInfo.getTableName() + "];";
	}

	/**
	 * Generate the sql to create a table in the database. 
	 * 
	 * @param dbset
	 * @return
	 */
	private String generateCreateSql(ClassInfo classInfo) {
		ArrayList<String> columns = new ArrayList<String>();
		for(FieldInfo field : classInfo.getBasicFields()) {
			if(field.isRowId()) continue; 
			String column = generateColumn(field);
			if(field.isPrimaryKey()) {
				if(field.isGenerated())
					column += " PRIMARY KEY AUTOINCREMENT";
				else
					column += " PRIMARY KEY";
			}
			columns.add(column);
		}
		String columnList = TextUtils.join(", ", columns);
		return String.format("create table %s (%s)", classInfo.getTableName(), columnList);
	}
	
	/**
	 * Generate the sql for a single column in the database. 
	 * 
	 * @param field
	 * @return
	 */
	private String generateColumn(FieldInfo field) {
		return "[" + field.getName() + "] " + getSqlDataType(field.getType()); 
	}
	
	private String getSqlDataType(Class<?> type) {
		if(	
			type.isAssignableFrom(int.class) || 
			type.isAssignableFrom(long.class) || 
			type.isAssignableFrom(boolean.class)
		) {
			return "INTEGER";
		}
		if(type.isAssignableFrom(String.class)) {
			return "TEXT";
		}
		if(type.isAssignableFrom(Date.class)) {
			return "INTEGER";
		}
		if(type.isAssignableFrom(byte[].class)) {
			return "BLOB";
		}
		if(
			type.isAssignableFrom(float.class) || 
			type.isAssignableFrom(double.class)
		) {
			return "REAL";
		}
		throw new RuntimeException("Could not generate a db column for " + type.getSimpleName());
	}
}
