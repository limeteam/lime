package com.trashlabs.lime.converters;

import java.lang.reflect.Field;


import android.content.ContentValues;
import android.database.Cursor;

public class BooleanConverter extends TConverter {

	public BooleanConverter(Field field) {
		super(field);
	}
	
	@Override
	public void frObj(ContentValues values, String columnName, Object item)
	throws IllegalArgumentException, IllegalAccessException, InstantiationException {
		values.put(columnName, (field.getBoolean(item)) ? 1 : 0);
	}
	@Override
	public void toObj(Cursor cursor, int columnIndex, Object item) 
	throws IllegalArgumentException,IllegalAccessException, InstantiationException {
		field.setBoolean(item, (cursor.getInt(columnIndex) == 1) ? true : false);
	}

}
