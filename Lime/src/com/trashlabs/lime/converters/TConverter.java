package com.trashlabs.lime.converters;

import java.lang.reflect.Field;
import java.sql.Date;

import android.content.ContentValues;
import android.database.Cursor;

public abstract class TConverter {
	
	Field field; 
	
	public TConverter(Field field) {
		this.field = field; 
	}

	public abstract void toObj(Cursor cursor, int columnIndex, Object item) 
	throws IllegalArgumentException, IllegalAccessException, InstantiationException;
	
	public abstract void frObj(ContentValues values, String columnName, Object item)
	throws IllegalArgumentException, IllegalAccessException, InstantiationException;
	
	public static TConverter get(Field field) {
		// this is a kludge that needs fixing, but it'll work for now
		if(field.getType().isAssignableFrom(boolean.class))
			return new BooleanConverter(field);
		else if (field.getType().isAssignableFrom(Date.class))
			return new DateConverter(field);
		else if (field.getType().isAssignableFrom(double.class))
			return new DoubleConverter(field);
		else if (field.getType().isAssignableFrom(float.class))
			return new FloatConverter(field);
		else if (field.getType().isAssignableFrom(int.class))
			return new IntegerConverter(field);
		else if (field.getType().isAssignableFrom(long.class))
			return new LongConverter(field);
		else if (field.getType().isAssignableFrom(short.class))
			return new ShortConverter(field);
		else if (field.getType().isAssignableFrom(String.class))
			return new StringConverter(field);
		else
			return null;
	}
}
