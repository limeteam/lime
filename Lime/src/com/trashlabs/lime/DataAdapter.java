package com.trashlabs.lime;

import java.util.List;

import com.trashlabs.lime.DataEntity;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class DataAdapter<T extends DataEntity> extends BaseAdapter {
	
	List<T> items; 
	
	public DataAdapter(List<T> items) {
		this.items = items;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public T getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return items.get(position)._rowid_;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getView(position, convertView, parent, getItem(position));
	}
	
	public abstract View getView(int position, View convertView, ViewGroup parent, T item);
}