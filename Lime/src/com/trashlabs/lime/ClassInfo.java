package com.trashlabs.lime;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Hashtable;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.trashlabs.lime.annotations.Table;

/**
 * ClassInfo holds a cached set of FieldInfo. This class represents basic information about a class
 * with details for reading and writing. 
 */
class ClassInfo {
	
	private ArrayList<FieldInfo> fields; 
	private ArrayList<FieldInfo> basicFields;
	private Hashtable<String, FieldInfo> byName; 
	private String tableName; 
	private Class<?> type;
	
	/**
	 * Get the data type for this class.
	 * @return
	 */
	public Class<?> getType() {
		return type; 
	}
	
	/**
	 * Get the table name for sqlite for this class. 
	 * @return
	 */
	public String getTableName() {
		return tableName; 
	}
	
	/**
	 * Get all of the fields in this class. 
	 * @return
	 */
	public ArrayList<FieldInfo> getFields() {
		return fields; 
	}
	
	/**
	 * Get a field info by name. 
	 * 
	 * @param name
	 * @return
	 */
	public FieldInfo getFieldByName(String name) {
		return byName.get(name);
	}
	
	/**
	 * Return all of the basic fields. Basic is defined as Java intrinsics plus strings. 
	 * 
	 * @return
	 */
	public ArrayList<FieldInfo> getBasicFields() {
		return basicFields; 
	}
	
	/**
	 * Get a string array of all the basic fields. Basic is defined as Java intrinsics plus strings.
	 * 
	 * @return
	 */
	public String[] getBasicFieldNames() {
		String[] names = new String[basicFields.size()];
		for(int i = 0; i < basicFields.size(); i++)
			names[i] = tableName + "." + basicFields.get(i).getName() + " AS " + basicFields.get(i).getName();
		return names;
	}
	
	/**
	 * Get a string array of all the basic field names prepended with a table alias. Basic is defined as Java 
	 * intrinsics plus strings.The result will be something in the format "tableAlias.fieldName AS 
	 * tableAliasfieldName"
	 * 
	 * @param tableAlias
	 * @return
	 */
	public String[] getBasicFieldNames(String tableAlias) {
		String[] names = new String[basicFields.size()];
		for(int i = 0; i < basicFields.size(); i++)
			names[i] = tableAlias + "." + basicFields.get(i).getName() + " AS " + tableAlias + basicFields.get(i).getName();
		return names;
	}
	
	/**
	 * Construct a new ClassInfo based on a type. This constructor is package private. Callers should use
	 * the Schema class to get a new ClassInfo to take advantage of caching therein.
	 * 
	 * @param type
	 */
	ClassInfo(Class<?> type) {
		this.type = type; 
		this.tableName = (type.isAnnotationPresent(Table.class)) ? type.getAnnotation(Table.class).value() : type.getSimpleName();
		this.fields = new ArrayList<FieldInfo>();
		this.basicFields = new ArrayList<FieldInfo>();
		this.byName = new Hashtable<String, FieldInfo>();
		for(Field field : type.getFields()) {
			FieldInfo info = new FieldInfo(field);
			fields.add(info);
			if(info.isBasic()) basicFields.add(info);
			byName.put(info.getName(), info);
		}
	}
	
	/**
	 * This method will traverse the model hierarchy and return a reader which is capable of reading 
	 * objects from the supplied cursor. 
	 * 
	 * @param cursor
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends DataEntity> ObjectReader<T> getReader(Cursor cursor) {
		return new ObjectReader<T>(cursor, (Class<T>) getType());
	}
	
	/**
	 * This method will traverse the model hierarchy and return a reader which is capable of reading 
	 * objects from the supplied cursor. 
	 * 
	 * @param cursor
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends DataEntity> ObjectReader<T> getReader(SQLiteDatabase db, Cursor cursor) {
		ObjectReader<T> reader = new ObjectReader<T>(cursor, (Class<T>)getType());
		return reader;
	}
}
