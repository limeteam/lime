package com.trashlabs.lime;

import java.util.concurrent.ConcurrentHashMap;

/**
 * The Schema holds a static map of class and relationship to cache all of the class and field probing 
 * information. Once a class is cached, the cached version will be accessed. 
 */
class Schema {
	
	private static ConcurrentHashMap<Class<?>, ClassInfo> classes = new ConcurrentHashMap<Class<?>, ClassInfo>();
	private static ConcurrentHashMap<String, ClassInfo> byName = new ConcurrentHashMap<String, ClassInfo>();
	
	public static ClassInfo getInfo(Class<?> type) {
		ClassInfo info = classes.get(type);
		if(info == null) {
			info = new ClassInfo(type);
			classes.put(type, info);
			byName.put(info.getTableName(), info);
		}
		return info; 
	}
	
	public static ClassInfo getInfo(String tableName) {
		return byName.get(tableName);
	}

}
